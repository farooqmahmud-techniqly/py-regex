import unittest
import re


class RegexTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_can_match_single_line(self):
        regex = "<td data-column-index=\"(\d{1,3})\" sorttable_customkey=\".*\">(.*)</td>"
        subject = "<td data-column-index=\"30\" sorttable_customkey=\"george.wingo@whatever.com\"><a href=\"mailto:george.wingo@whatever.com\">george.wingo@whatever.com</a></td>"

        self.__assert_match(regex, subject, "30", "george.wingo@whatever.com</a>")

    def test_can_match_multi_line(self):
        regex = "<td data-column-index=\"(\\d{1,3})\" sorttable_customkey=\".*\\n.*\">(.*\\n.*)</td>"
        subject = "<td data-column-index=\"73\" sorttable_customkey=\"1231 Main St ne #886\nredmond, wa 98052\">1231 Main St ne #886\nredmond, wa 98052</td>"

        self.__assert_match(regex, subject, "73", "1231 Main St ne #886\nredmond, wa 98052")
        compiled_regex = re.compile(regex)
        match_obj = compiled_regex.search(subject)

    def test_can_match_empty_value(self):
        regex = "<td data-column-index=\"(\d{1,3})\" sorttable_customkey=\".*\">(.*)</td>"
        subject = "<td data-column-index=\"80\" sorttable_customkey=\"\"></td>"

        self.__assert_match(regex, subject, "80", "")

    def __assert_match(self, regex, subject, *expected_group_values):
        compiled_regex = re.compile(regex)
        match_obj = compiled_regex.search(subject)

        self.assertIsNotNone(match_obj)

        group_number = 0

        for v in expected_group_values:
            self.assertEqual(match_obj.group(group_number + 1), expected_group_values[group_number])
            group_number += 1

    if __name__ == "__main__":
        unittest.main()
